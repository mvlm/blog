import api from "../http";
import { IUser } from "../types/user-type";
import { AxiosResponse } from "axios";
import { AuthResponse } from "../types/auth-response";

export default class UserService {
  static async updateUser(
    userId: number,
    firstName: string,
    lastName: string,
    email: string,
    picture?: any
  ): Promise<AxiosResponse<IUser>> {
    const formData = new FormData();
    formData.append("userId", userId.toString());
    formData.append("firstName", firstName);
    formData.append("lastName", lastName);
    formData.append("email", email);
    if (picture) {
      console.log("picture");
      formData.append("picture", picture);
    }

    return api.put<IUser>("/auth/update", formData);
  }
  static async resetPassword(
    email: string,
    password: string,
    token: string
  ): Promise<AxiosResponse<AuthResponse>> {
    return api.put<AuthResponse>(
      "/auth/reset-password",
      { email, password },
      {
        headers: token ? { RESET_PASS_TOKEN: token } : {},
      }
    );
  }
}
