import { createAsyncThunk } from "@reduxjs/toolkit";
import AuthService from "../../../services/auth-service";
import axios from "axios";
import { AuthResponse } from "../../../types/auth-response";
import { API_URL } from "../../../http";
import UserService from "../../../services/user-service";

export interface authArgs {
  type: "login" | "register" | "checkAuth" | "resetPassword";
  firstName?: string;
  lastName?: string;
  email?: string;
  password?: string;
  resetPasswordToken?: string;
}

export const authorizeUser = createAsyncThunk(
  "auth/login",
  async (args: authArgs, thunkAPI) => {
    try {
      let response;
      if (args.type === "login") {
        response = await AuthService.login(args.email!, args.password!);
      } else if (args.type === "register") {
        response = await AuthService.registration(
          args.firstName!,
          args.lastName!,
          args.email!,
          args.password!
        );
      } else if (args.type === "checkAuth") {
        response = await axios.get<AuthResponse>(`${API_URL}/auth/refresh`, {
          withCredentials: true,
        });
      }
      return response?.data;
    } catch (e) {
      return thunkAPI.rejectWithValue(e);
    }
  }
);

export const logoutUser = createAsyncThunk(
  "auth/logout",
  async (args, thunkAPI) => {
    try {
      await AuthService.logout();
    } catch (e) {
      return thunkAPI.rejectWithValue(e);
    }
  }
);

export const resetPassword = createAsyncThunk(
  "auth/resetPassword",
  async (args: authArgs, thunkAPI) => {
    try {
      const response = await UserService.resetPassword(
        args.email!,
        args.password!,
        args.resetPasswordToken!
      );
      return response?.data;
    } catch (e) {
      return thunkAPI.rejectWithValue(e);
    }
  }
);
