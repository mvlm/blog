import React, { FC, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import "../login/login.scss";
import Button from "../../components/common/button/Button";
import FormGroup from "../../components/common/formGroup/FormGroup";
import { CircularProgress } from "@mui/material";
import { Navigate } from "react-router-dom";
import { useAppDispatch, useAppSelector, useTitle } from "../../hooks";
import { useForm } from "react-hook-form";
import { setError } from "../../store/reducers/auth/authSlice";
import Alert from "@mui/material/Alert";
import { parseQueryParams } from "../../helpers/queryParams";
import {
  resetPassword,
  authArgs,
} from "../../store/reducers/auth/actionCreators";

const ResetPassword: FC = () => {
  const [isSuccess, setSuccess] = useState(false);
  const { search } = useLocation();
  const queryParams = parseQueryParams(search);
  const { isLoading, error, isAuth } = useAppSelector((state) => state.auth);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const dispatch = useAppDispatch();
  useTitle("Reset password");

  useEffect(() => {
    dispatch(setError(""));
  }, [dispatch]);

  const onSubmit = async (data: any) => {
    const args: authArgs = {
      type: "resetPassword",
      email: data.Email,
      password: data.Password,
      resetPasswordToken: queryParams.token,
    };
    const { payload: { isAxiosError = false } = {} as any } = await dispatch(
      resetPassword(args)
    );
    if (!isAxiosError) {
      setSuccess(!isAxiosError);
    }
  };

  if (isAuth) {
    return <Navigate to={"/"} />;
  }

  return (
    <div className={"login"}>
      <h2 className={"loginTitle"}>Reset Password</h2>
      {error && !isSuccess && !isSuccess && (
        <div className={"registerError"}>{error}</div>
      )}
      {isSuccess ? (
        queryParams.token ? (
          <Alert severity="success">Password was updated.</Alert>
        ) : (
          <Alert severity="success">
            We will send you email. Please check it and follow instructions.
          </Alert>
        )
      ) : (
        <div className={"loginForm"}>
          <form onSubmit={handleSubmit(onSubmit)}>
            {!queryParams.token && (
              <FormGroup
                fieldName={"Email"}
                register={register}
                errors={errors}
                placeholder={"Enter email..."}
                isRequired={true}
              />
            )}
            {queryParams.token && (
              <FormGroup
                fieldName={"Password"}
                register={register}
                errors={errors}
                placeholder={"Enter password..."}
                isRequired={true}
                type={"password"}
              />
            )}
            <Button
              type={"submit"}
              progress={
                isLoading ? (
                  <CircularProgress style={{ color: "white" }} size={20} />
                ) : null
              }
              text={"Reset"}
            />
          </form>
        </div>
      )}
    </div>
  );
};

export default ResetPassword;
