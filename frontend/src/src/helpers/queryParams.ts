export const parseQueryParams = (search: string) =>
  search
    .substr(1)
    .split('&')
    .reduce((qs: Record<any, any>, query) => {
      const chunks = query.split('=');
      const key = chunks[0];
      let value: string | boolean | number = decodeURIComponent(
        chunks[1] ?? '',
      );
      const valueLower = value.trim().toLowerCase();

      if (valueLower === 'true' || value === 'false') {
        value = Boolean(value);
      }

      // eslint-disable-next-line no-param-reassign,no-return-assign,no-sequences
      return (qs[key] = value), qs;
    }, {});
