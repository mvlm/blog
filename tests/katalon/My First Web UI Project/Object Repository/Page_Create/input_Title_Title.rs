<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Title_Title</name>
   <tag></tag>
   <elementGuidId>44409e42-6f80-47f9-88fe-2bb8eb214e3b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='Title']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;Title&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e94f19e0-7463-41b6-9ca8-4b26b1b210e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Enter title...</value>
      <webElementGuid>3c381093-4b59-4ace-96dd-7e9865095298</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Title</value>
      <webElementGuid>3051f932-67d8-415c-9b9d-bfcb01dafd02</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;layout&quot;]/div[@class=&quot;main&quot;]/div[@class=&quot;mainWrapper&quot;]/div[@class=&quot;createPost&quot;]/div[@class=&quot;postInner&quot;]/div[@class=&quot;formGroup&quot;]/input[1]</value>
      <webElementGuid>c2593e35-c872-498d-95e7-53133c01a0c0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='Title']</value>
      <webElementGuid>42cddb55-2d0c-4163-ab7a-e525057224cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/div/div/div[2]/input</value>
      <webElementGuid>b2c05b47-f79e-44fa-9913-04ebb95406b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/input</value>
      <webElementGuid>2da530af-b40b-4d07-b80f-896da9db5f4b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Enter title...' and @name = 'Title']</value>
      <webElementGuid>966c2fd3-2b3b-4c22-ba9e-ed2ef3b99391</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
