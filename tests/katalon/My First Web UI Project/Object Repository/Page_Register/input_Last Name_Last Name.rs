<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Last Name_Last Name</name>
   <tag></tag>
   <elementGuidId>627fa2cf-c79c-4b9b-94a8-39bd365ad203</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='Last Name']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;Last Name&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>c15e1077-2498-445b-a0c7-22135afb0711</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Enter last name...</value>
      <webElementGuid>ba687f07-ce84-436f-8000-e996240c5e70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Last Name</value>
      <webElementGuid>b99e5d2e-adba-4fc7-a324-7ca5955e6a17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;layout&quot;]/div[@class=&quot;main&quot;]/div[@class=&quot;mainWrapper&quot;]/div[@class=&quot;login&quot;]/div[@class=&quot;loginForm&quot;]/form[1]/div[@class=&quot;formGroup&quot;]/input[1]</value>
      <webElementGuid>468ba169-beef-44f5-ba87-a194874e0f1a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='Last Name']</value>
      <webElementGuid>2c6c31e6-ebaa-4e25-86cc-a5a6a98dc47c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/div/div/form/div[2]/input</value>
      <webElementGuid>71ec7930-7c51-41b0-84eb-8bb4c79c9f45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/input</value>
      <webElementGuid>5ec45de7-5e2f-4a23-90a7-7ea402e03451</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Enter last name...' and @name = 'Last Name']</value>
      <webElementGuid>2c0d5f02-4d7b-49a5-84e7-bee32b7e5f56</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
