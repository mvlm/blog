<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_First Name_First Name</name>
   <tag></tag>
   <elementGuidId>58253860-07b9-41fe-9ed3-b09e2a76b39a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='First Name']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;First Name&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>f604ee9d-7105-46c9-8a74-2ec79aaef82a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Enter first name...</value>
      <webElementGuid>8cec6ae8-e330-4710-ac1b-bbcb4ecbd638</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>First Name</value>
      <webElementGuid>55c3dcea-4780-44cf-8dea-df02076b24c5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;layout&quot;]/div[@class=&quot;main&quot;]/div[@class=&quot;mainWrapper&quot;]/div[@class=&quot;login&quot;]/div[@class=&quot;loginForm&quot;]/form[1]/div[@class=&quot;formGroup&quot;]/input[1]</value>
      <webElementGuid>f2a54497-95fa-4f52-8adc-49d2690288d3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='First Name']</value>
      <webElementGuid>3604c37c-8096-4810-8bf1-3313e6fcd741</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/div/div/form/div/input</value>
      <webElementGuid>86397a80-a9a9-4263-8c6d-dc0a8dfb8a65</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>792467c0-3619-488b-971a-2fb5e2f84a06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@placeholder = 'Enter first name...' and @name = 'First Name']</value>
      <webElementGuid>275ae437-06f0-41cd-8697-7821b2ad1b47</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
