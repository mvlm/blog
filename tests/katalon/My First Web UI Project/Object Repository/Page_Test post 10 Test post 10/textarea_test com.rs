<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_test com</name>
   <tag></tag>
   <elementGuidId>843bf11d-eaca-473c-b1b8-ce551f6e0db1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/div/div/div/div[2]/div/textarea</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>textarea.commentFormArea</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>c06f6e6b-17ab-400b-b108-2c581c47bef6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Share your expressions...</value>
      <webElementGuid>f00a7fc4-5032-4941-896b-7fa1bdacb252</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>commentFormArea</value>
      <webElementGuid>e14df86b-c44b-4d34-8f34-013cf457aede</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>test com</value>
      <webElementGuid>2ca06286-ddd4-4bac-9eda-67ac3ca30cae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;layout&quot;]/div[@class=&quot;main&quot;]/div[@class=&quot;mainWrapper&quot;]/div[@class=&quot;postWrapper&quot;]/div[@class=&quot;postInner&quot;]/div[@class=&quot;postComments&quot;]/div[@class=&quot;commentForm&quot;]/textarea[@class=&quot;commentFormArea&quot;]</value>
      <webElementGuid>7473afbd-9b34-40fa-9b76-b28ab3ea9292</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/div/div/div[2]/div/textarea</value>
      <webElementGuid>8e72c81f-c251-4886-9a94-bd0fa439c1f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[1]/following::textarea[1]</value>
      <webElementGuid>8cedad30-1db3-46b4-b4e7-ef6b191ac6ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test post 10 Test post 10'])[2]/following::textarea[1]</value>
      <webElementGuid>ee761bc8-ec21-4b46-8d2f-7f4272bf4a1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Send'])[1]/preceding::textarea[1]</value>
      <webElementGuid>3b8ee547-eaea-44af-853d-37b0ba00cd63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No comments yet'])[1]/preceding::textarea[1]</value>
      <webElementGuid>625bc3c5-fc91-4006-8d00-32861b553b1a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='test com']/parent::*</value>
      <webElementGuid>e3fa25d4-c507-40db-a032-1c579b84dac9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>a4766dd0-5e96-4bd2-9f52-ac00a473a77b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@placeholder = 'Share your expressions...' and (text() = 'test com' or . = 'test com')]</value>
      <webElementGuid>ba084e0e-7d3f-4ea7-8b42-c82f8ad227ce</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
