<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_test</name>
   <tag></tag>
   <elementGuidId>5b180b22-e43a-4fac-9b25-6849ac91f0b6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/div/div/div/div[2]/div/textarea</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>textarea.commentFormArea</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>9894afc5-4a73-4852-9453-34c9da94745d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Share your expressions...</value>
      <webElementGuid>5dcb0a9c-e81a-44ca-aaef-14ecd33d342e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>commentFormArea</value>
      <webElementGuid>ac60d339-550b-4c15-a81d-f37af6b0445b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>test</value>
      <webElementGuid>313f65fe-a73b-4739-9f46-ca41c7044973</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;layout&quot;]/div[@class=&quot;main&quot;]/div[@class=&quot;mainWrapper&quot;]/div[@class=&quot;postWrapper&quot;]/div[@class=&quot;postInner&quot;]/div[@class=&quot;postComments&quot;]/div[@class=&quot;commentForm&quot;]/textarea[@class=&quot;commentFormArea&quot;]</value>
      <webElementGuid>13b72018-c36e-4386-9e58-d08d1d65cca2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/div/div/div[2]/div/textarea</value>
      <webElementGuid>aae1f3c4-4ec4-418a-91cc-defee2e2af38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[1]/following::textarea[1]</value>
      <webElementGuid>46482a1b-7600-4107-8b39-8cb19b33c8c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test post 10 Test post 10'])[2]/following::textarea[1]</value>
      <webElementGuid>1f2d9bab-2b9e-4ace-9806-38447e2acbce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Send'])[1]/preceding::textarea[1]</value>
      <webElementGuid>a27f2c9f-f22e-43fd-b34b-f7434797f124</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No comments yet'])[1]/preceding::textarea[1]</value>
      <webElementGuid>a61435a7-d299-4c68-b114-113a0c97ab7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='test']/parent::*</value>
      <webElementGuid>31f445ff-c0c7-4d2e-b9be-cbd051ca6e7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>23727632-07b1-4a6d-bc31-2d2db3397bcd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@placeholder = 'Share your expressions...' and (text() = 'test' or . = 'test')]</value>
      <webElementGuid>18304ab2-590c-42d4-93b7-3fb07ac73c87</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
