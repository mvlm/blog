<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_test co</name>
   <tag></tag>
   <elementGuidId>994d0259-c0f0-48ca-b514-64098c8d8ed4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/div/div/div/div[2]/div/textarea</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>textarea.commentFormArea</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>e7666fb4-78ab-45b3-80a1-e3b800a7f483</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Share your expressions...</value>
      <webElementGuid>600bfb17-b2ed-4d47-b4a1-9306d0ee8c28</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>commentFormArea</value>
      <webElementGuid>c755cae4-9785-4fc5-8f9e-591ca1a0b1b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>test co</value>
      <webElementGuid>2644d907-c49c-4ede-b5af-9cc985b75904</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;layout&quot;]/div[@class=&quot;main&quot;]/div[@class=&quot;mainWrapper&quot;]/div[@class=&quot;postWrapper&quot;]/div[@class=&quot;postInner&quot;]/div[@class=&quot;postComments&quot;]/div[@class=&quot;commentForm&quot;]/textarea[@class=&quot;commentFormArea&quot;]</value>
      <webElementGuid>bb61a565-de39-41e4-8a29-888ee2d1f4fa</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/div/div/div[2]/div/textarea</value>
      <webElementGuid>95ccc194-eb47-4bdb-a7d0-5d8188bfd083</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[1]/following::textarea[1]</value>
      <webElementGuid>b8c2ed4c-88fe-424b-9ed9-c1ee81b34517</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test post 10 Test post 10'])[2]/following::textarea[1]</value>
      <webElementGuid>342ecaba-6e63-46d8-b661-bd858b4a5889</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Send'])[1]/preceding::textarea[1]</value>
      <webElementGuid>ca2b7d6b-b89a-49f0-9fd9-5c65066a95c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No comments yet'])[1]/preceding::textarea[1]</value>
      <webElementGuid>0a6f91ac-13e4-4a5a-9d8f-cda3f04f0510</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='test co']/parent::*</value>
      <webElementGuid>a02042ed-d7c0-44dd-8a8a-38a7975c7d1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>f80f8aaa-0413-4246-b499-9778d88dcb19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@placeholder = 'Share your expressions...' and (text() = 'test co' or . = 'test co')]</value>
      <webElementGuid>a94e191b-8a15-4dbe-a7e2-602155d619cc</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
