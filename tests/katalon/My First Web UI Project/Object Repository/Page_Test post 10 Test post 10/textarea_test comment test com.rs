<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_test comment test com</name>
   <tag></tag>
   <elementGuidId>b0e1cb1c-9ca8-43ac-bc33-6e4b13cae80a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/div/div/div/div[2]/div/textarea</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>textarea.commentFormArea</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
      <webElementGuid>7aadb712-2265-4a13-8ad7-dfa7741a1356</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Share your expressions...</value>
      <webElementGuid>599241e8-9890-4173-9729-d0e9f741ffe8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>commentFormArea</value>
      <webElementGuid>f27421e1-e23e-400e-937d-96180fdd9e6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>test comment test com</value>
      <webElementGuid>435cd833-3fca-4456-870c-2da9526af0fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;layout&quot;]/div[@class=&quot;main&quot;]/div[@class=&quot;mainWrapper&quot;]/div[@class=&quot;postWrapper&quot;]/div[@class=&quot;postInner&quot;]/div[@class=&quot;postComments&quot;]/div[@class=&quot;commentForm&quot;]/textarea[@class=&quot;commentFormArea&quot;]</value>
      <webElementGuid>c409d3e2-1ad2-463c-915b-81c372c37fbe</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/div/div/div[2]/div/textarea</value>
      <webElementGuid>d0642943-7cf9-44f4-b6ce-2fcdb2dd1445</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Comments'])[1]/following::textarea[1]</value>
      <webElementGuid>de8f9000-944d-4082-9106-3d8d1d6f2032</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Test post 10 Test post 10'])[2]/following::textarea[1]</value>
      <webElementGuid>6bd68c66-7b88-40c2-b048-6b70770eb025</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Send'])[1]/preceding::textarea[1]</value>
      <webElementGuid>880c3997-4a6b-4687-8e95-086758eaf175</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No comments yet'])[1]/preceding::textarea[1]</value>
      <webElementGuid>aadaabf6-4245-49ff-ac24-adba0a1524dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='test comment test com']/parent::*</value>
      <webElementGuid>0c3aea12-5141-4a8d-a361-1ac225ad6909</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//textarea</value>
      <webElementGuid>f0a45dd6-5c79-4692-b890-38e01e6e8e71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//textarea[@placeholder = 'Share your expressions...' and (text() = 'test comment test com' or . = 'test comment test com')]</value>
      <webElementGuid>5a47f653-ddfd-4fa6-b415-97826047de2b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
