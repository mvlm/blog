<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Make Post</name>
   <tag></tag>
   <elementGuidId>aac7f43f-56d7-4a25-b45d-d6f776884427</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/div/div/div/div[2]/ul/li[2]/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.menu.bottom > ul > li:nth-of-type(2) > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>91a86bbf-6f29-4a04-b0b7-d76e1517d923</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Make Post</value>
      <webElementGuid>f2a263ed-4377-4776-b6ab-34df6267a67f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[@class=&quot;layout&quot;]/div[@class=&quot;main&quot;]/div[@class=&quot;mainWrapper&quot;]/div[@class=&quot;home&quot;]/div[@class=&quot;sidebar&quot;]/div[@class=&quot;menu bottom&quot;]/ul[1]/li[2]/span[1]</value>
      <webElementGuid>5ac390ac-4496-40a9-8632-22e9533ebcc9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/div/div/div[2]/ul/li[2]/span</value>
      <webElementGuid>11d43f73-bee6-4771-b522-40817769c5d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tags'])[1]/following::span[1]</value>
      <webElementGuid>4c18c822-4c30-4baf-8924-b8973933d360</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Profile'])[1]/following::span[2]</value>
      <webElementGuid>74295cb0-e81b-4230-ba15-cd2210f9abe5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About'])[1]/preceding::span[1]</value>
      <webElementGuid>bdb1e372-850f-4f3e-a10c-a00c41089d36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contact'])[1]/preceding::span[2]</value>
      <webElementGuid>c086e43e-d252-497f-be31-934c3c761a05</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Make Post']/parent::*</value>
      <webElementGuid>d9ebfece-3b95-464b-8e9a-ba5242fc7053</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/ul/li[2]/span</value>
      <webElementGuid>6eac4525-bbb9-429c-a7a8-a6d5760306ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Make Post' or . = 'Make Post')]</value>
      <webElementGuid>2c3be45b-c930-4efb-a630-2dfb90914f72</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
