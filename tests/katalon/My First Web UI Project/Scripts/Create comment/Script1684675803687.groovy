import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://localhost/')

WebUI.click(findTestObject('Object Repository/Page_MyBlog/button_Log in'))

WebUI.setText(findTestObject('Object Repository/Page_Log in/input_Email_Email'), 'test10@example.com')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Log in/input_Password_Password'), 'RigbBhfdqOBGNlJIWM1ClA==')

WebUI.click(findTestObject('Object Repository/Page_Log in/button_Continue'))

WebUI.click(findTestObject('Object Repository/Page_MyBlog/span_0 Comments'))

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_t'), 't')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_te'), 'te')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_tes'), 'tes')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test'), 'test')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test_1'), 'test ')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test c'), 'test c')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test co'), 'test co')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test com'), 'test com')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test comm'), 'test comm')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test comme'), 'test comme')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test commen'), 'test commen')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test comment'), 'test comment')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test comment_1'), 'test comment ')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test comment t'), 'test comment t')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test comment te'), 'test comment te')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test comment tes'), 'test comment tes')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test comment test'), 'test comment test')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test comment test_1'), 'test comment test ')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test comment test c'), 'test comment test c')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test comment test co'), 'test comment test co')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test comment test com'), 'test comment test com')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test comment test comm'), 'test comment test comm')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test comment test comme'), 'test comment test comme')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test comment test commen'), 'test comment test commen')

WebUI.setText(findTestObject('Object Repository/Page_Test post 10 Test post 10/textarea_test comment test comment'), 'test comment test comment')

WebUI.click(findTestObject('Object Repository/Page_Test post 10 Test post 10/button_Send'))

WebUI.closeBrowser()

