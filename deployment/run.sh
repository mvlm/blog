#!/usr/bin/env bash

set -e

export USER=$(id -u)
export GROUP=$(id -g)

docker compose -p ${COMPOSE_PROJECT_NAME} -f docker-compose.yml -f docker-compose-local.yml "$@"
