#!/usr/bin/env bash

set -ex

export USER=$(id -u)
export GROUP=$(id -g)

docker compose -p ${COMPOSE_PROJECT_NAME} build

docker run --rm -i -v ${PWD}/../api/src:/mnt ${COMPOSE_PROJECT_NAME}-api bash <<EOF
set -ex
rsync --chown=${USER}:${GROUP} --delete --force --stats -am /app/node_modules/ /mnt/node_modules/
EOF

docker run --rm -i -v ${PWD}/../frontend/src:/mnt ${COMPOSE_PROJECT_NAME}-frontend bash <<EOF
set -ex
rsync --chown=${USER}:${GROUP} --delete --force --stats -am /app/node_modules/ /mnt/node_modules/
EOF

cp docker-compose-local.yml.tpl docker-compose-local.yml

# https://docker-sync.readthedocs.io/en/latest/getting-started/installation.html
docker compose -p ${COMPOSE_PROJECT_NAME} -f docker-compose.yml -f docker-compose-local.yml up -d
