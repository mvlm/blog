import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import * as bcrypt from 'bcryptjs';
import { TokenService } from '../token/token.service';
import { UserDto } from '../user/dto/user.dto';
import { UserService } from '../user/user.service';
import { LoginUserDto } from '../user/dto/login.user.dto';

@Injectable()
export class SecurityService {
  constructor(private tokenService: TokenService, private userService: UserService) {}

  async registration(userDto: UserDto) {
    const candidate = await this.userService.getByEmail(userDto.email);
    if (candidate) {
      throw new HttpException(`User with email: ${userDto.email} already exists`, HttpStatus.BAD_REQUEST);
    }
    const hashedPassword = await bcrypt.hash(userDto.password, 5);
    const user = await this.userService.createUser({
      ...userDto,
      password: hashedPassword,
    });
    const tokens = this.tokenService.generateTokens(user);
    await this.tokenService.saveToken(user.id, tokens.refreshToken);

    return {
      ...tokens,
      user: { ...user, posts: [] },
    };
  }

  async login(loginUserDto: LoginUserDto) {
    const user = await this.userService.getByEmail(loginUserDto.email);
    if (!user) {
      throw new HttpException(`User with email: ${loginUserDto.email} not found`, HttpStatus.NOT_FOUND);
    }
    const passwordMatch = await bcrypt.compare(loginUserDto.password, user.password);
    if (!passwordMatch) {
      throw new HttpException('Password is invalid', HttpStatus.UNAUTHORIZED);
    }
    const tokens = this.tokenService.generateTokens(user);
    await this.tokenService.saveToken(user.id, tokens.refreshToken);
    return {
      ...tokens,
      user,
    };
  }

  async logout(refreshToken: string) {
    await this.tokenService.removeToken(refreshToken);
  }

  async refresh(refreshToken) {
    if (!refreshToken) {
      throw new HttpException('Token is not provided', HttpStatus.UNAUTHORIZED);
    }
    const userData = await this.tokenService.validateRefreshToken(refreshToken);
    const tokenFromDb = await this.tokenService.findToken(refreshToken);
    if (!userData || !tokenFromDb) {
      throw new HttpException('Token is invalid', HttpStatus.UNAUTHORIZED);
    }
    const user = await this.userService.getById(tokenFromDb.user.id);
    const tokens = this.tokenService.generateTokens(user);
    await this.tokenService.saveToken(user.id, tokens.refreshToken);

    return {
      ...tokens,
      user,
    };
  }

  async resetPassword(token: string, loginUserDto: LoginUserDto) {
    if (token) {
      const userData = await this.tokenService.validatePasswordResetToken(token);
      const tokenFromDb = await this.tokenService.findPasswordResetToken(token);

      if (!userData || !tokenFromDb) {
        throw new HttpException('User not found or token expired', HttpStatus.UNAUTHORIZED);
      }

      if (!loginUserDto.password) {
        throw new HttpException(`You have to set new password`, HttpStatus.BAD_REQUEST);
      }

      await this.userService.resetPassword(tokenFromDb.user.id, await bcrypt.hash(loginUserDto.password, 5));
      await this.tokenService.removeToken(token);
      throw new HttpException('Password was updated', HttpStatus.ACCEPTED);
    } else {
      const user = await this.userService.getByEmail(loginUserDto.email);

      if (!user) {
        throw new HttpException('User not found', HttpStatus.NOT_FOUND);
      }

      const newToken = this.tokenService.generatePasswordResetToken(user);
      await this.tokenService.savePasswordResetToken(user.id, newToken);

      // const transporter = nodemailer.createTransport({
      //   // Configure your email transport settings
      //   service: 'gmail',
      //   auth: {
      //     user: process.env.EMAIL_USERNAME,
      //     pass: process.env.EMAIL_PASSWORD,
      //   },
      // });

      const mailOptions = {
        from: 'noreply@example.com',
        to: user.email,
        subject: 'Password Reset',
        text: `You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n
             Please click on the following link, or paste it into your browser to complete the process within one hour of receiving it:\n\n
             ${process.env.CLIENT_URL}/reset-password?token=${newToken} \n\n
             If you did not request this, please ignore this email and your password will remain unchanged.\n`,
      };

      // await transporter.sendMail(mailOptions);
      console.log(mailOptions);
    }
    return 'OK';
  }
}
