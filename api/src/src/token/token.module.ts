import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { TokenService } from './token.service';
import { Token } from './token.entity';
import { UserModule } from '../user/user.module';

@Module({
  imports: [JwtModule.register({}), TypeOrmModule.forFeature([Token]), UserModule],
  providers: [TokenService],
  exports: [TokenService],
})
export class TokenModule {}
