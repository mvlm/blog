import { Repository } from 'typeorm';
import { UserService } from './user.service';
import { User } from './user.entity';
import { FileService } from '../file/file.service';
import { UserDto } from './dto/user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

let userService: UserService;
let userRepository: Repository<User>;
let fileService: FileService;

const mockRepository = () => ({
  save: jest.fn(),
  find: jest.fn(),
  findOne: jest.fn(),
  update: jest.fn(),
});

const mockFileService = () => ({
  createFile: jest.fn(),
});

const userDto: UserDto = {
  firstName: 'John',
  lastName: 'Doe',
  email: 'john.doe@example.com',
  password: 'password123',
};

const updateUserDto: UpdateUserDto = {
  userId: 1,
  firstName: 'UpdatedFirstName',
  lastName: 'UpdatedLastName',
  email: 'updated.john.doe@example.com',
};

const file: Express.Multer.File = {
  originalname: 'picture.jpg',
  filename: 'picture.jpg',
  mimetype: 'image/jpeg',
  size: 10000,
  buffer: Buffer.from(''),
} as any;

const updatedUser = {
  id: 1,
  firstName: 'UpdatedFirstName',
  lastName: 'UpdatedLastName',
  email: 'updated.john.doe@example.com',
  password: 'password123',
  profilePicture: 'new-picture-path',
};

beforeEach(() => {
  userRepository = mockRepository() as any;
  fileService = mockFileService() as any;
  userService = new UserService(userRepository, fileService);
});

describe('UserService', () => {
  it('should create a user', async () => {
    const user = {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      email: 'john.doe@example.com',
      password: 'encrypted_password',
    };

    (userRepository.save as any).mockImplementation(() => Promise.resolve(user));

    const result = await userService.createUser(userDto);
    expect(result).toEqual(user);
    expect(userRepository.save).toHaveBeenCalledWith(userDto);
  });

  it('should get all users', async () => {
    const users = [
      {
        id: 1,
        firstName: 'John',
        lastName: 'Doe',
        email: 'john.doe@example.com',
        password: 'password123',
      },
      {
        id: 2,
        firstName: 'Jane',
        lastName: 'Doe',
        email: 'jane.doe@example.com',
        password: 'password123',
      },
    ];

    (userRepository.find as any).mockImplementation(() => Promise.resolve(users));

    const result = await userService.getAllUsers();
    expect(result).toEqual(users);
    expect(userRepository.find).toHaveBeenCalledWith({
      relations: ['posts', 'comments'],
    });
  });

  it('should get a user by ID', async () => {
    const user = {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      email: 'john.doe@example.com',
      password: 'password123',
    };

    (userRepository.findOne as any).mockImplementation(() => Promise.resolve(user));

    const result = await userService.getById(1);
    expect(result).toEqual(user);
    expect(userRepository.findOne).toHaveBeenCalledWith({
      where: { id: 1 },
      relations: ['posts'],
    });
  });

  it('should get a user by email', async () => {
    const user = {
      id: 1,
      firstName: 'John',
      lastName: 'Doe',
      email: 'john.doe@example.com',
      password: 'password123',
    };

    (userRepository.findOne as any).mockImplementation(() => Promise.resolve(user));

    const result = await userService.getByEmail('john.doe@example.com');
    expect(result).toEqual(user);
    expect(userRepository.findOne).toHaveBeenCalledWith({
      where: { email: 'john.doe@example.com' },
      relations: ['posts'],
    });
  });
});

describe('updateUser', () => {
  it('should update a user with a new profile picture', async () => {
    (fileService.createFile as any).mockImplementation(() => Promise.resolve('new-picture-path'));
    (userRepository.update as any).mockImplementation(() => Promise.resolve(updatedUser));
    (userRepository.findOne as any).mockImplementation(() => Promise.resolve(updatedUser));

    const result = await userService.updateUser(updateUserDto, file);

    expect(result).toEqual(updatedUser);
    // expect(fileService.createFile).toHaveBeenCalledWith(file);
    expect(userRepository.update).toHaveBeenCalledWith(updateUserDto.userId, {
      firstName: updateUserDto.firstName,
      lastName: updateUserDto.lastName,
      email: updateUserDto.email,
      // profilePicture: 'new-picture-path',
    });
    expect(userRepository.findOne).toHaveBeenCalledWith({
      where: { id: updateUserDto.userId },
      relations: ['posts'],
    });
  });

  it('should update a user without a new profile picture', async () => {
    (userRepository.update as any).mockImplementation(() => {});
    (userRepository.findOne as any).mockImplementation(() => Promise.resolve(updatedUser));

    const result = await userService.updateUser(updateUserDto, file);

    expect(result).toEqual(updatedUser);
    expect(fileService.createFile).not.toHaveBeenCalled();
    expect(userRepository.update).toHaveBeenCalledWith(updateUserDto.userId, {
      firstName: updateUserDto.firstName,
      lastName: updateUserDto.lastName,
      email: updateUserDto.email,
    });
    expect(userRepository.findOne).toHaveBeenCalledWith({
      where: { id: updateUserDto.userId },
      relations: ['posts'],
    });
  });
});
