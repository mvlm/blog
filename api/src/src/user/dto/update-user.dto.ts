// eslint-disable-next-line max-classes-per-file
import { IsEmail, IsNotEmpty, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';

export class UpdateUserDto {
  @IsNotEmpty()
  readonly userId: number;

  @IsString()
  @IsNotEmpty()
  readonly firstName: string;

  @IsString()
  @IsNotEmpty()
  readonly lastName: string;

  @IsNotEmpty()
  @IsEmail()
  @IsString()
  readonly email: string;
}

export class ResetPasswordDto {
  @IsNotEmpty()
  @IsEmail()
  @IsString()
  @IsOptional()
  readonly email: string;

  @MinLength(6, { message: 'Minimum length of password must be 6' })
  @MaxLength(20, { message: 'Maximum length of password is 20' })
  @IsString()
  @IsOptional()
  readonly password: string;
}
