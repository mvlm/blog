import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { UserDto } from './dto/user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { FileService } from '../file/file.service';

@Injectable()
export class UserService {
  constructor(@InjectRepository(User) private usersRepository: Repository<User>, private fileService: FileService) {}

  async createUser(userDto: UserDto | User): Promise<User> {
    return this.usersRepository.save(userDto);
  }

  async updateUser(updateUserDto: UpdateUserDto, file: Express.Multer.File) {
    let picturePath;
    if (file && 'picture' in file) {
      const { picture } = file;
      picturePath = await this.fileService.createFile(picture[0]);
    }
    const { firstName } = updateUserDto;
    const { lastName } = updateUserDto;
    const { email } = updateUserDto;
    if (picturePath) {
      await this.usersRepository.update(updateUserDto.userId, {
        firstName,
        lastName,
        email,
        profilePicture: picturePath,
      });
    } else {
      await this.usersRepository.update(updateUserDto.userId, {
        firstName,
        lastName,
        email,
      });
    }

    return this.getById(updateUserDto.userId);
  }

  async resetPassword(userID: number, password: string) {
    await this.usersRepository.update(userID, {
      password,
    });

    return this.getById(userID);
  }

  async getAllUsers() {
    return this.usersRepository.find({
      relations: ['posts', 'comments'],
    });
  }

  async getById(userId: number) {
    return this.usersRepository.findOne({
      where: { id: userId },
      relations: ['posts'],
    });
  }

  async getByEmail(email: string) {
    return this.usersRepository.findOne({
      where: { email },
      relations: ['posts'],
    });
  }
}
