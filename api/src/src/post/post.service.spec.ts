import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PostService } from './post.service';
import { Post } from './post.entity';
import { PostDto } from './dto/post.dto';
import { UpdatePostDto } from './dto/update.post.dto';
import { UserService } from '../user/user.service';
import { FileService } from '../file/file.service';
import { TagService } from '../tag/tag.service';

describe('PostService', () => {
  let postService: PostService;
  let postRepository: Repository<Post>;

  const fileServiceMock = {
    createFile: jest.fn().mockImplementation(() => Promise.resolve('test.jpg')),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PostService,
        {
          provide: getRepositoryToken(Post),
          useValue: {
            findOne: jest.fn(),
            save: jest.fn(),
            find: jest.fn(),
            delete: jest.fn(),
          },
        },
        {
          provide: UserService,
          useValue: {},
        },
        {
          provide: FileService,
          useValue: {},
        },
        {
          provide: TagService,
          useValue: {},
        },
        { provide: FileService, useValue: fileServiceMock },
      ],
    }).compile();

    postService = module.get<PostService>(PostService);
    postRepository = module.get<Repository<Post>>(getRepositoryToken(Post));
  });

  it('should be defined', () => {
    expect(postService).toBeDefined();
  });

  // describe('createPost', () => {
  //   it('should create a new post', async () => {
  //     const postDto: PostDto = {
  //       title: 'Test Post',
  //       text: 'This is a test post',
  //       userId: 1,
  //     };
  //
  //     const newPost = new Post();
  //     newPost.title = postDto.title;
  //     newPost.text = postDto.text;
  //     newPost.postImage = 'test.jpg';
  //     newPost.user = postDto.userId;
  //     newPost.dateAndTimePublish = new Date();
  //
  //     const mockFiles = {
  //       picture: {
  //         filename: 'test.jpg',
  //       },
  //     };
  //
  //     (postRepository.save as jest.Mock).mockResolvedValue(newPost);
  //
  //     // Mock getPostById to return the newPost object
  //     const getPostById = jest.spyOn(postService, 'getPostById');
  //     getPostById.mockResolvedValue(newPost);
  //
  //     const result = await postService.createPost(postDto, mockFiles);
  //
  //     expect(result).toEqual(newPost);
  //     expect(postRepository.save).toHaveBeenCalledWith(newPost);
  //     expect(getPostById).toHaveBeenCalledWith(newPost.id);
  //   });
  // });

  describe('getPostById', () => {
    it('should return a post by its ID', async () => {
      const postId = 1;

      const existingPost = new Post();
      existingPost.id = postId;
      existingPost.title = 'Test Post';
      existingPost.text = 'This is a test post';

      (postRepository.findOne as jest.Mock).mockResolvedValue(existingPost);

      const result = await postService.getPostById(postId);

      expect(result).toEqual(existingPost);
      expect(postRepository.findOne).toHaveBeenCalledWith({
        where: { id: postId },
        relations: ['comments', 'userLikes'],
      });
    });
  });

  describe('updatePost', () => {
    it('should update a post', async () => {
      const postId = 1;
      const postImage = 'test.jpg';
      const updatePostDto: UpdatePostDto = {
        postId,
        title: 'Updated Test Post',
        text: 'This is an updated test post',
        postImage,
        tags: null,
      };

      const existingPost = new Post();
      existingPost.id = postId;
      existingPost.title = 'Test Post';
      existingPost.text = 'This is a test post';
      existingPost.postImage = postImage;
      existingPost.tags = null;

      const updatedPost = new Post();
      updatedPost.id = postId;
      updatedPost.title = updatePostDto.title;
      updatedPost.text = updatePostDto.text;
      updatedPost.postImage = postImage;
      updatedPost.tags = null;

      const mockFiles = {
        picture: {
          filename: postImage,
        },
      };

      (postRepository.findOne as jest.Mock).mockResolvedValue(existingPost);
      (postRepository.save as jest.Mock).mockResolvedValue(updatedPost);

      const result = await postService.updatePost(updatePostDto, mockFiles);

      expect(result).toEqual(updatedPost);
      expect(postRepository.findOne).toHaveBeenCalledWith({
        where: { id: postId },
        relations: ['comments', 'userLikes'],
      });
      expect(postRepository.save).toHaveBeenCalledWith(updatedPost);
    });
  });

  describe('deletePost', () => {
    it('should delete a post by its ID', async () => {
      const postId = 1;
      const post = new Post();
      post.id = postId;

      jest.spyOn(postService, 'getPostById').mockResolvedValue(post);
      jest.spyOn(postRepository, 'delete');

      await postService.deletePost(postId);

      expect(postRepository.delete).toHaveBeenCalledWith(postId);
    });
  });
});
