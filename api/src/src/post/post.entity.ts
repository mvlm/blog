import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
// eslint-disable-next-line import/no-cycle
import { User } from '../user/user.entity';
// eslint-disable-next-line import/no-cycle
import { Comment } from '../comment/commnet.entity';
// eslint-disable-next-line import/no-cycle
import { Like } from '../like/like.entity';
// eslint-disable-next-line import/no-cycle
import { Tag } from '../tag/tag.entity';

@Entity({ name: 'posts' })
export class Post {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  text: string;

  @Column()
  dateAndTimePublish: Date;

  @OneToMany(() => Like, like => like.post, { eager: true })
  userLikes: Like[];

  @Column({ nullable: true })
  postImage?: string;

  @ManyToOne(() => User, user => user.posts, { eager: true, onDelete: 'CASCADE' })
  user: number;

  @OneToMany(() => Comment, comment => comment.post, { eager: true })
  comments: Comment[];

  @ManyToMany(() => Tag, { eager: true })
  @JoinTable()
  tags: Tag[];
}
