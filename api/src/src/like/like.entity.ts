import { Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
// eslint-disable-next-line import/no-cycle
import { User } from '../user/user.entity';
// eslint-disable-next-line import/no-cycle
import { Post } from '../post/post.entity';

@Entity({ name: 'likes' })
export class Like {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, user => user.likedPosts, { eager: true })
  user: number;

  @ManyToOne(() => Post, post => post.userLikes, { onDelete: 'CASCADE' })
  post: number;
}
