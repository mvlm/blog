# About

This is example of client-server blog application built in React⚛️ and NestJS🦄.

## App features:
* Authentication/Authorization system
* Password reset
* Creating posts
* Editing user profile
* View list of posts
* View single post
* View latest posts
* Ordering posts
* Creating comments
* Validation of fields
* Likes system
* Mobile responsive

## Stack of technologies used for building this app:
React, NestJS, Redux, PostgreSQL, SCSS, TypeScript, Axios, Postman, JWT and so on

### How to run development environment:

* install [direnv](https://direnv.net/docs/installation.html) tool
* install [docker engine](https://docs.docker.com/engine/install/)
* install [docker-compose plugin](https://docs.docker.com/compose/install/)
* go to `deployment` directory, run: `cd deployment`
* copy `.envrc.example` to `.envrc`, run: `cp .envrc.example .envrc`
* export env variables, run: `direnv allow`
* run `./deploy.sh`

### How to use deployment

You can use `deployment/run.sh` script to run compose wrapped commands. It takes all the same arguments which you can set for compose.
Example:
```
cd deployment
./deploy.sh
./run.sh ps -a
./run.sh exec -u ${UID}:${GRP} -it api bash
./run.sh exec -u ${UID}:${GRP} -it frontend bash
./run.sh logs -f
./run.sh logs -f api
./run.sh logs -f frontend
./run.sh down
```
